﻿using HBsis.Livraria.Domain.Entidades;
using HBsis.Livraria.Domain.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text;

namespace Hbsis.Livraria.Dominio
{
    public class LivrariaContext : DbContext
    {
        public LivrariaContext() : base("SqlServerContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<LivrariaContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LivrariaContext, Configuration>());
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Livraria");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); 
        }

        public DbSet<Livros> Livros { get; set; }
        public DbSet<Autor> Autor { get; set; }

    }
}
