namespace HBsis.Livraria.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Livraria.Livros",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Descricao = c.String(),
                        DataPublicacao = c.DateTime(nullable: false),
                        AutorID_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Livraria.Autor", t => t.AutorID_ID)
                .Index(t => t.AutorID_ID);
            
            CreateTable(
                "Livraria.Autor",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Livraria.Livros", "AutorID_ID", "Livraria.Autor");
            DropIndex("Livraria.Livros", new[] { "AutorID_ID" });
            DropTable("Livraria.Autor");
            DropTable("Livraria.Livros");
        }
    }
}
