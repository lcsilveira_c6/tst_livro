namespace HBsis.Livraria.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFKAutor : DbMigration
    {
        public override void Up()
        {
            DropIndex("Livraria.Livros", new[] { "AutorID_ID" });
            RenameColumn(table: "Livraria.Livros", name: "AutorID_ID", newName: "Autor_ID");
            AlterColumn("Livraria.Livros", "Autor_ID", c => c.Int(nullable: false));
            CreateIndex("Livraria.Livros", "Autor_ID");
        }
        
        public override void Down()
        {
            DropIndex("Livraria.Livros", new[] { "Autor_ID" });
            AlterColumn("Livraria.Livros", "Autor_ID", c => c.Int());
            RenameColumn(table: "Livraria.Livros", name: "Autor_ID", newName: "AutorID_ID");
            CreateIndex("Livraria.Livros", "AutorID_ID");
        }
    }
}
