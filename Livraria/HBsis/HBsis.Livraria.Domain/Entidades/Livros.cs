﻿using HBsis.Livraria.Domain.Entidades;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HBsis.Livraria.Domain.Entidades
{
    public class Livros
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }

        [Display(Name = "Nome do Livro")]
        public String Nome { get; set; }

        [ForeignKey("Autor")]
        [Required]
        public int Autor_ID { get; set; }
        public Autor Autor { get; set; }

        public String Descricao { get; set; }

        [Display(Name = "Data de Publicação")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DataPublicacao { get; set; }
    }
}
