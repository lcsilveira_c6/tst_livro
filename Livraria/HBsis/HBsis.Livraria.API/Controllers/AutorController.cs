﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HBsis.Livraria.Domain.Entidades;
using Hbsis.Livraria.Dominio;

namespace HBsis.Livraria.API.Controllers
{
    public class AutorController : ApiController
    {
        private LivrariaContext db = new LivrariaContext();

        // GET: api/Autor
        public IQueryable<Autor> GetAutor()
        {
            return db.Autor;
        }

        // GET: api/Autor/5
        [ResponseType(typeof(Autor))]
        public IHttpActionResult GetAutor(int id)
        {
            Autor autor = db.Autor.Find(id);
            if (autor == null)
            {
                return NotFound();
            }

            return Ok(autor);
        }

        // PUT: api/Autor/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAutor(int id, Autor autor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != autor.ID)
            {
                return BadRequest();
            }

            db.Entry(autor).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AutorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Autor
        [ResponseType(typeof(Autor))]
        public IHttpActionResult PostAutor(String NomeAutor)
        {

            if (NomeAutor == "" )
            {
                return BadRequest(ModelState);
            }
            Autor autor = new Autor
            {
                Nome = NomeAutor
            };

            db.Autor.Add(autor);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = autor.ID }, autor);
        }

        // DELETE: api/Autor/5
        [ResponseType(typeof(Autor))]
        public IHttpActionResult DeleteAutor(int id)
        {
            Autor autor = db.Autor.Find(id);
            if (autor == null)
            {
                return NotFound();
            }

            db.Autor.Remove(autor);
            db.SaveChanges();

            return Ok(autor);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AutorExists(int id)
        {
            return db.Autor.Count(e => e.ID == id) > 0;
        }
    }
}