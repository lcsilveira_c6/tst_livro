using System.Web.Http;
using WebActivatorEx;
using HBsis.Livraria.API;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace HBsis.Livraria.API
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "HBsis.Livraria.API");
                    })
                .EnableSwaggerUi(c => { });
        }
    }
}
